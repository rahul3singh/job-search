import React, { Component } from "react";
// import { fetchHomeJobs} from '../Actions/HomeJobList'
// import { connect } from "react-redux";
import axios from "axios";
import {
  Box,
  Input,
  Text,
  Flex,
  Spacer,
  InputGroup,
  InputRightElement,
  Button,
  Checkbox,
  CheckboxGroup,
  Radio,
  RadioGroup,
} from "@chakra-ui/core";
// import { Flex, Spacer } from "@chakra-ui/core"
import image from "./bg.jpg";

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    // fetch('https://jobs.github.com/positions.json?full_time=true&location=Berlin')
    // // .then(response => response.json())
    // .then(json => console.log(json))
    axios
      .get(
        `https://jobs.github.com/positions.json?full_time=true&location=Berlin`
      )
      .then((res) => {
        const persons = res.data;
        console.log(persons.json());
      });
  }

  getJobs(val) {
    console.log("changed", val);
    switch (val) {
      case "1":
        // axios
        //   .get(
        //     `https://jobs.github.com/positions.json?full_time=true&location=London`
        //   )
        //   .then((res) => {
        //     const persons = res.data;
        //     console.log(persons.json());
        //   });
        fetch(`https://jobs.github.com/positions.json?full_time=true&location=London`)
        .then((data)=>data)
        .then(d=>console.log(d))
        break;
      case "2":
        axios
          .get(
            `https://jobs.github.com/positions.json?full_time=true&location=Amersterdom`
          )
          .then((res) => {
            const persons = res.data;
            console.log(persons.json());
          });
        break;
      case "3":
        axios
          .get(
            `https://jobs.github.com/positions.json?full_time=true&location=New York`
          )
          .then((res) => {
            const persons = res.data;
            console.log(persons.json());
          });
        break;
      case "4":
        axios
          .get(
            `https://jobs.github.com/positions.json?full_time=true&location=Berlin`
          )
          .then((res) => {
            const persons = res.data;
            console.log(persons.json());
          });
        break;
    }
  }
  render() {
    return (
      <div>
        <Text w="80%" p={4} textAlign="left" mx={"auto"} my={"auto"}>
         GitHub Jobs
        </Text>
        <Box
          backgroundImage={`url(${image})`}
          alt="not"
          backgroundPosition="center"
          backgroundRepeat="no-repeat"
          w="80%"
          h="15vh"
          rounded="lg"
          pt={"30px"}
          mx={"auto"}
        >
          <InputGroup size="md" w="70%" mx={"auto"}>
            <Input pr="4.5rem" placeholder="search" />
            <InputRightElement width="4.5rem">
              <Button h="1.75rem" size="sm">
                Search
              </Button>
            </InputRightElement>
          </InputGroup>
        </Box>
        <Flex
          w="80%"
          mx={"auto"}
          justifyContent={"space-between"}
          mt="10px"
          minH="40vh"
          p={"0"}
        >
          <Flex
            p="1"
            // bg="red.400"
            width="35%"
            textAlign="start"
            flexDirection="column"
            justify="space-between"
          >
            <Checkbox defaultIsChecked>Full Time</Checkbox>
            <Text>Location</Text>
            <Input placeholder="Here is a sample placeholder" size="sm" />
            <RadioGroup
              defaultValue="1"
              onChange={(e) => this.getJobs(e.target.value)}
            >
              <Radio value="1">London</Radio>
              <Radio value="2">Amersterdom</Radio>
              <Radio value="3">New york</Radio>
              <Radio value="4">Berlin</Radio>
            </RadioGroup>
          </Flex>

          <Box p="4" bg="green.400" width="60%">
            Box 2
          </Box>
        </Flex>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  HomeJobs: state.HomeJobs.allHomeJobs,
});
// export default connect(mapStateToProps, {fetchHomeJobs})(HomePage);
export default HomePage;
