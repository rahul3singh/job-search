import React from "react";
import "./App.css";
import HomePage from "./components/HomePage";
import { Provider } from "react-redux";
import store from "./store";
// import {Heading} from '@chakra-ui/core'
function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <HomePage />
      </div>
    </Provider>
  );
}

export default App;
