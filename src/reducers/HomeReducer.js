import { HomeRadioJobList } from "../Actions/type";
const initialState = {
  allHomeJobs: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case HomeRadioJobList:
  
      return {
        ...state,
        allHomeJobs: action.payload,
      };
    default:
      return state;
  }
}
