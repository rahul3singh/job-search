import {HomeRadioJobList} from './type'

export const  fetchHomeJobs=(val)=>dispatch=>{
    // console.log(val)
        fetch('https://jobs.github.com/positions.json?full_time=true&'+'location='+val)
        .then(res=>res.json())
        .then(post=>dispatch({
            type:HomeRadioJobList,
            payload:post.items
        })) 
}

